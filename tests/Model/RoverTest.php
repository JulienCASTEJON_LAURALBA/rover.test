<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use \App\Model\Rover;
use \App\Model\Heading;
use \App\Model\Plateau;
use \App\Model\Position;
use \App\Interfaces\iInstruction;
use \App\Model\Instruction;
use \App\Exceptions\HeadingException;
use \App\Exceptions\RoverPositionException;
use \App\Exceptions\RoverMoveException;

final class RoverTest extends TestCase
{

}
