<?php

namespace App\Interfaces;

interface iHeading
{
    const HEADING_NORTH = 'N';
    const HEADING_EAST = 'E';
    const HEADING_SOUTH = 'S';
    const HEADING_WEST = 'W';

    public function __construct($initial);

    public function rotateLeft();

    public function rotateRight();

    public function asString(): string;
}
